package airhockey;

import org.newdawn.slick.geom.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;

public class Puck {
    BodyDef bd;
    Body body;
    FixtureDef fd;
    Fixture f;
    Vec2 pos;
    Vec2 dispenseForce;
    float r;
    boolean isBeingDispensed;
    
    org.newdawn.slick.geom.Shape gShape;
    org.jbox2d.collision.shapes.Shape pShape;
    
    public void update(){
        pos = body.getPosition();
        gShape.setX((pos.x*50-r));
        gShape.setY((pos.y*50-r));
        if (this.getClass() != Mallet.class){
            if (pos.x*50 < AirHockey.left.r-AirHockey.dispensingBorder-25) {
                update(new Vec2(AirHockey.left.r-AirHockey.dispensingBorder-20,AirHockey.resY/2).mul(1/50.0f));
                body.setLinearVelocity(new Vec2());
                body.applyForceToCenter(AirHockey.dispensingForce);
            }
            if (pos.x*50 > AirHockey.right.l+AirHockey.dispensingBorder+25){
                update(new Vec2(AirHockey.right.l+AirHockey.dispensingBorder+20,AirHockey.resY/2).mul(1/50.0f));
                body.setLinearVelocity(new Vec2());
                body.applyForceToCenter(AirHockey.dispensingForce.negate());
            }
            if (pos.x*50-r > AirHockey.left.r && pos.x*50+r < AirHockey.right.l) isBeingDispensed = false;
        }
    }
    public void update(Vec2 pos){
        gShape.setX((pos.x*50-r));
        gShape.setY((pos.y*50-r));
        body.setTransform(pos,0.0f);
    }
    public Puck(Vec2 pos){
        r = AirHockey.rad;
        
        gShape = new Circle(pos.x-r,pos.y-r,r);
        pShape = new CircleShape();
        
        bd = new BodyDef();
        bd.position.set(pos.x/50,pos.y/50);
        bd.type = BodyType.DYNAMIC;
        bd.linearDamping = 0.9f;
        bd.bullet = true;
        
        pShape.m_radius = r/50;
        fd = new FixtureDef();
        fd.shape = pShape;
        fd.density = 0.6f;
        fd.friction = 1.0f;
        fd.restitution = 0.5f;
        //fd.filter.maskBits = mask;
        body = AirHockey.world.createBody(bd);
        f = body.createFixture(fd);
        body.setUserData(this);
        isBeingDispensed = true;
    }
}
