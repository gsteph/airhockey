/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package airhockey;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.*;

public class Wall {
    PolygonShape ps;
    FixtureDef fd;
    BodyDef bd;
    Body body;
    float t,d,l,r;
    public Wall(World w, float posX, float posY, float width, float height){
        t = posY;
        d = t+height;
        l = posX;
        r = l+width;
        ps = new PolygonShape();
        ps.setAsBox(width/50,height/50);
        fd = new FixtureDef();
        fd.shape = ps;
        fd.density = 1.0f;
        fd.friction = 0.3f;
        bd = new BodyDef();
        bd.position.set(posX/50, posY/50);
        bd.type = BodyType.STATIC;
        body = w.createBody(bd);
        body.createFixture(fd);
        body.setUserData(this);
    }
}
