package airhockey;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

public class CollisionHandler implements ContactListener {
    int sa,sb;
    Wall a,b;
    Mallet p1,p2;
    ArrayList<Puck> p;
    NetworkThread networkThread;
    Object[] o;
    public CollisionHandler(ArrayList<Puck> p, Mallet p1, Mallet p2, Wall a, Wall b){
        sa=sb=0;
        this.a = a;
        this.b = b;
        this.p1 = p1;
        this.p2 = p2;
        this.p = p;
        o = new Object[2];
    }
    private static boolean check(Contact contact, Class<?> objA, Class<?> objB, Object[] o){
        Object cA = contact.getFixtureA().getBody().getUserData();
        Object cB = contact.getFixtureB().getBody().getUserData();
        if (cA.getClass() == objA && cB.getClass() == objB) {
            o[0] = cA;
            o[1] = cB;
            return true;
        }
        if (cA.getClass() == objB && cB.getClass() == objA) {
            o[0] = cB;
            o[1] = cA;
            return true;
        }
        return false;
    }
    
    @Override
    public void beginContact(Contact contact) {
        if (check(contact,Goal.class,Puck.class,o)){
            ((Goal)o[0]).score++;
            ((Goal)o[0]).active = true;
            AirHockey.puckRemovalQueue.add((Puck)o[1]);
            //AirHockey.puckDispenserQueue[0] += 3;
            //AirHockey.puckDispenserQueue[1] += 3;
        }
    }

    @Override
    public void endContact(Contact contact) {
        contact.getFixtureA().getBody().setAngularVelocity(0.0f);
        contact.getFixtureB().getBody().setAngularVelocity(0.0f);
        int tmp=0;
        if (check(contact,Puck.class,Mallet.class,o)) tmp=1;
        if (check(contact,Puck.class,Puck.class,o)) tmp=2;
        if (tmp>0){
            if (AirHockey.online){
                try {
                    for (int i = 0; i < tmp; i++){
                        networkThread.oos.writeByte(1);
                        networkThread.oos.writeFloat(((Puck)o[i]).body.getPosition().x);
                        networkThread.oos.writeFloat(((Puck)o[i]).body.getPosition().y);
                        networkThread.oos.writeFloat(((Puck)o[i]).body.getLinearVelocity().x);
                        networkThread.oos.writeFloat(((Puck)o[i]).body.getLinearVelocity().y);
                        networkThread.oos.writeByte(AirHockey.puck.indexOf(o[i]));
                    }
                } catch (IOException ex) {
                    Logger.getLogger(CollisionHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        if (check(contact,Wall.class,Puck.class,o)){
            if (((Puck)o[1]).isBeingDispensed) contact.setEnabled(false);
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }
    
}
