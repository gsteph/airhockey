package airhockey;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jbox2d.common.Vec2;
import org.newdawn.slick.Image;

public class NetworkThread implements Runnable{
    ServerSocket s;
    Socket c;
    Thread t;
    boolean client=false;
    boolean tcp = true;
    InetAddress ip;
    ObjectOutputStream oos;
    ObjectInputStream ois;
    Vec2 buf;
   
    public NetworkThread(String ip) throws UnknownHostException{
        this.ip = ip==null?null:InetAddress.getByName(ip);
        t = new Thread(this,"Network Thread");
    }
    
    @Override
    public void run() {
        try {
            if (ip != null){
                c = new Socket(ip,AirHockey.port);
                Image tmp = AirHockey.imgMallet2;
                AirHockey.imgMallet2 = AirHockey.imgMallet;
                AirHockey.imgMallet = tmp;
                for (Puck p : AirHockey.puck) AirHockey.puckRemovalQueue.add(p);
                client = true;
            }else{
                s = new ServerSocket(AirHockey.port);
                AirHockey.isWaiting = true;
                c = s.accept();
                for (Puck p : AirHockey.puck) AirHockey.puckRemovalQueue.add(p);
            }
            c.setTcpNoDelay(true);
            oos = new ObjectOutputStream(c.getOutputStream());
            ois = new ObjectInputStream(c.getInputStream());
            AirHockey.online = true;
            AirHockey.puckDispenserQueue[0]++;
            AirHockey.iMilestone = 0;
            AirHockey.goali = 0;
            AirHockey.goalu.score = AirHockey.goald.score = 0;
            AirHockey.seconds = AirHockey.matchDuration;
            while(true){
                Vec2 pos = new Vec2();
                Vec2 vel = new Vec2();
                Byte b = ois.readByte();
                pos.x = AirHockey.resX/50.0f - ois.readFloat();
                pos.y = AirHockey.resY/50.0f - ois.readFloat();
                vel.x = ois.readFloat();
                vel.y = ois.readFloat();
                if (b == 0){
                    if (AirHockey.goali == 0){
                        AirHockey.mallet2.update(pos);
                        AirHockey.mallet2.body.setLinearVelocity(vel.negate());
                        AirHockey.mallet2.body.setAngularVelocity(0.0f);
                    }
                }else{
                    int idx = ois.readByte();
                    AirHockey.puck.get(idx).update(pos);
                    AirHockey.puck.get(idx).body.setLinearVelocity(vel.negate());
                    AirHockey.puck.get(idx).body.setAngularVelocity(0.0f);
                }
            }
        } catch (IOException ex) {
            AirHockey.isWaiting = false;
            AirHockey.online = false;
            Logger.getLogger(NetworkThread.class.getName()).log(Level.SEVERE, null, ex);
            //if ("socket closed".equals(ex.getMessage())) return;
        }
    }
    
}
