package airhockey;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Filter;

public class Mallet extends Puck{
    public Mallet(Vec2 pos, boolean ghost){
        super(pos);
        if (ghost){
            Filter filter = new Filter();
            filter.maskBits = 0;
            this.f.setFilterData(filter);
        }
    }
   
}
